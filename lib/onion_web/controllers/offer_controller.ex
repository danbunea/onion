defmodule OnionWeb.OfferController do
  use OnionWeb, :controller
  alias Onion.Marketing

  def show(conn, %{"id" => id}) do
    case Integer.parse(id) do
      {_, ""} ->
        offer = Marketing.get_offer(id)

        if offer != nil do
          Marketing.update_offer_requests(offer.id)

          if offer.active do
            render(conn, "show.html", offer: offer)
          else
            conn
            |> put_flash(:error, "The offer expired")
            |> put_status(400)
            |> render("show.html", offer: nil)
          end
        else
          conn
          |> put_flash(:error, "The id you provided cannot be found")
          |> put_status(404)
          |> render("show.html", offer: nil)
        end

      _ ->
        conn
        |> put_flash(:error, "The id you provided is invalid")
        |> put_status(400)
        |> render("show.html", offer: nil)
    end
  end
end
