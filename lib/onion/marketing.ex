defmodule Onion.Marketing do
  @moduledoc """
  The Marketing context.
  """

  import Ecto.Query, warn: false
  alias Onion.Repo

  alias Onion.Marketing.Offer

  @doc """
  Gets a single offer.

  Raises `Ecto.NoResultsError` if the Offer does not exist.

  ## Examples

      iex> get_offer(123)
      %Offer{}

      iex> get_offer(456)
      nil

  """
  def get_offer(id), do: Repo.get(Offer, id)

  @doc """
  Updates an offer's number of requests.

  ## Examples

      iex> update_offer_requests(1)
      {:ok, %Offer{...}}

  """
  def update_offer_requests(id) do
    case get_offer(id) do
      %Offer{} = offer ->
        offer
        |> Offer.changeset(%{requests: offer.requests + 1})
        |> Repo.update()

      nil ->
        nil
    end
  end
end
