defmodule Onion.Marketing.Offer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "offers" do
    field :percentage, :integer
    field :active, :boolean, default: true
    field :requests, :integer, default: 0

    timestamps()
  end

  @doc false
  def changeset(offer, attrs) do
    offer
    |> cast(attrs, [:percentage, :active, :requests])
    |> validate_required([:percentage])
  end
end
