defmodule OnionWeb.OfferControllerTestData do
  def percentage(), do: 15
  def error_400(), do: "The id you provided is invalid"
  def error_400_offer_expired(), do: "The offer expired"
  def error_404(), do: "The id you provided cannot be found"
end
