defmodule OnionWeb.OffersControllerTest do
  use OnionWeb.ConnCase
  alias Onion.Marketing.Offer
  alias Onion.Repo
  import Mox
  alias OnionWeb.OfferControllerTestData, as: Data

  setup :verify_on_exit!

  describe "offer page should " do
    test "show a percentage for an offer when exits", %{conn: conn} do
      {:ok, offer} =
        %Offer{}
        |> Offer.changeset(%{percentage: Data.percentage()})
        |> Repo.insert()

      conn = get(conn, Routes.offer_path(conn, :show, offer.id))
      offer_after = Repo.get!(Offer, offer.id)

      assert html_response(conn, 200) =~ "#{Data.percentage()}%"
      assert 1 == offer_after.requests
    end

    test "shows an error when the id is valid but the offer doesn't exists", %{conn: conn} do
      conn = get(conn, Routes.offer_path(conn, :show, -404))
      assert html_response(conn, 404) =~ Data.error_404()
    end

    test "shows an error when the id is invalid", %{conn: conn} do
      conn = get(conn, Routes.offer_path(conn, :show, "a400a"))
      assert html_response(conn, 400) =~ Data.error_400()
    end

    test "shows an error when the offer expired", %{conn: conn} do
      {:ok, offer} =
        %Offer{}
        |> Offer.changeset(%{percentage: Data.percentage(), active: false})
        |> Repo.insert()

      conn = get(conn, Routes.offer_path(conn, :show, offer.id))
      offer_after = Repo.get!(Offer, offer.id)

      assert html_response(conn, 400) =~ Data.error_400_offer_expired()
      assert 1 == offer_after.requests
    end
  end
end
