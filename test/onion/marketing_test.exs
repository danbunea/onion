defmodule Onion.MarketingTest do
  use Onion.DataCase
  alias Onion.Marketing
  alias Onion.Repo

  describe "offers" do
    alias Onion.Marketing.Offer

    @valid_attrs %{percentage: 42}

    def offer_fixture() do
      {:ok, offer} =
        %Offer{}
        |> Offer.changeset(@valid_attrs)
        |> Repo.insert()

      offer
    end

    test "get_offer/1 returns the offer with given id" do
      offer = offer_fixture()
      assert Marketing.get_offer(offer.id) == offer
    end

    test "get_offer/-1 returns nil" do
      assert Marketing.get_offer(-1) == nil
    end

    test "update_offer requests" do
      offer = offer_fixture()
      {:ok, result} = Marketing.update_offer_requests(offer.id)
      assert 1 == result.requests
    end

    test "fails to update_offer requests returning nil" do
      offer = offer_fixture()
      result = Marketing.update_offer_requests(-1)
      assert nil == result
    end
  end
end
