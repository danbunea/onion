# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :onion,
  ecto_repos: [Onion.Repo]

# Configures the endpoint
config :onion, OnionWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "GKF2W8+pxZ/GEE6YOPf5uBW6af5B51tc8y6Wl9JFxyy7fRqQfdonV+dGSou5hvTi",
  render_errors: [view: OnionWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Onion.PubSub,
  live_view: [signing_salt: "V1N5LpYw"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
