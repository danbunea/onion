# Why the clean/onion/hexagonal architecture? A study in Elixir/Phoenix

You work for a company, on a project, and the project grows and changes, and you need to be on the top of it. Do you think the clean/onion/hexagonal architecture can help?

I designed an exercise, which will be solved in two ways. One , pretty traditional, and one using the clean architecture. Then we'll compare to see the different tradeoffs of the two solutions.

The exercise is composed of several steps, where you;re asked to build/change/add something to the application. 

We are a marketing agency and we're in charge of the Offers/Marketing module of our software. So let's see what we need to do.

## Step 1

**THE PROBLEM**

A page presenting the percentage of a specific order (specified by id) or why we couldn't see it: 

```
 /offers/:offer-id
```

![200](extra/200.png)

Only integer ids
![400](extra/400.png)

The offer is expired
![400](extra/400_expired.png)

If it;s not found
![404](extra/404.png)

IMPORTANT: every time an existing offer is requested, we increase it's requested number. Active or not.

**THE PLAN**

We will use a UI controller that will receive all the client's requests. He will talk to a Persistance manager (in charge of storing and retrieving stuff from a database) and considering some business rules he will return the right answer to the client (as HTML).

![404](extra/part I-sol I.png)

# Onion

To start your Phoenix server:

  * Setup the project with `mix setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
