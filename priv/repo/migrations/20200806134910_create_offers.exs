defmodule Onion.Repo.Migrations.CreateOffers do
  use Ecto.Migration

  def change do
    create table(:offers) do
      add :percentage, :integer

      timestamps()
    end
  end
end
