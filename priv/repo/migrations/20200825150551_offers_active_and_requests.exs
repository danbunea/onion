defmodule Onion.Repo.Migrations.OffersActiveAndRequests do
  use Ecto.Migration

  def change do
    alter table(:offers) do
      add :active, :boolean, default: true
      add :requests, :integer, default: 0
    end
  end
end
